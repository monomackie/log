#pragma once

#include <string>
#include <cstdio>

class Log {
public:
	static std::string getCurrentDateTime();
    static void info(const std::string& msg);
    static void info(const char *format, ...);
    static void warning(const std::string& msg);
    static void warning(const char *format, ...);
    static void error(const std::string& msg);
    static void error(const char *format, ...);
    static void debug(const std::string& msg);
    static void debug(const char *format, ...);
	static void printStackTrace(const std::exception& e, int level =  0);
};
