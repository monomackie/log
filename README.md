# Log

Library to log information belonging to 4 categories : 
* INFO
* DEBUG
* WARNING
* ERROR

The output is time stamped, and a newline character is appended at the end of the message. \
For example, the following code :
``` c++
int a = 16;
Log::debug("Hello");
Log::warning("World %d", a);
Log::info("<SENSOR_CONNECTED> Sensor connected to the device !");
std::Exception nestedError = ...;
Log::printStackTrace(nestedError);
```
prints 
```
[2021-11-03.10:50:46][DEBUG]: Hello
[2021-11-03.10:50:46][WARNING]: World 16
[2021-11-03.10:50:46][INFO]: <SENSOR_CONNECTED> Sensor connected to the device !
[2021-11-03.10:50:47][ERROR]: D:\WorldLoader.cpp:35: Could not instantiate object
     D:\ObjectLoader.cpp:59: Object does not exist
     D:\FileLoader.cpp:42: File not found
```

In our case :  
* error is used to print the content of the thrown exceptions
* debug is used for work which is in progress, but should be removed when commiting for master
* warning is currently unused
* info is used to display status information

info must always be used in combination with an additional tag to specify the nature of the information 
beeing logged

In addition, this lib also provides a printStackTrace function that unfolds and prints the specified nested exception

